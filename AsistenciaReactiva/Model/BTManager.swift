//
//  BTManager.swift
//  AsistenciaReactiva
//
//  Created by Jorge Andres Gomez Villamizar on 8/13/19.
//  Copyright © 2019 Jorge Andres Gomez Villamizar. All rights reserved.
//

import Foundation
import CoreBluetooth

class BTManager: NSObject, CBPeripheralManagerDelegate {
    var mainPeripheral:CBPeripheralManager? = nil
    
    //MARK: Overrides
    override init() {
        super.init()
        mainPeripheral = CBPeripheralManager(delegate: self, queue: nil);
    }
    
    //MARK: CBPeripheralManagerDelegate implementation
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        print("Change in status for peripheral manager");
        print(peripheral.state.rawValue)
        switch(peripheral.state){
        case CBManagerState.poweredOn:
            print("poweredOn")
        case CBManagerState.poweredOff:
            print("poweredOff")
        case CBManagerState.unauthorized:
            print("unauthorized")
        case CBManagerState.unsupported:
            print("unsupported")
        default: break
        }
    }
    
    func peripheralManagerDidStartAdvertising(_ peripheral: CBPeripheralManager, error: Error?) {
        if(error != nil) {
            print("There was an error while starting to advertise")
            print(error!)
        }
        
        print("Success starting advertising")
    }
    
    
    //MARK: Core functionalities
    func startDiscover(_ serviceName: String) {
        let advertisementData = [CBAdvertisementDataLocalNameKey: serviceName]
        print("Started advertising")
        mainPeripheral?.startAdvertising(advertisementData)
    }
    
    func endDiscover() {
        mainPeripheral?.stopAdvertising()
        if(mainPeripheral!.isAdvertising) {
            print("Did not stop advertising")
        } else {
            print("Did stop! Congratulations")
        }
    }
    
    func isAdvertising() -> Bool {
        return mainPeripheral!.isAdvertising
    }
}
