//
//  FireBaseManage.swift
//  AsistenciaReactiva
//
//  Created by Jorge Andres Gomez Villamizar on 8/14/19.
//  Copyright © 2019 Jorge Andres Gomez Villamizar. All rights reserved.
//

import Foundation

class FireBaseManager {
    let NOT_FOUND = "NOT_FOUND"
    let API_ROUTE = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/"
    
    //callback is only called if the student is attending the class
    func checkForValue(_ studentCode: String, _ callback: @escaping () -> Void) {
        var isAttending = false
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-M-yyyy"
        let dateString = formatter.string(from: date)
        
        let url = URL(string: self.API_ROUTE + "attendance/\(dateString)/students/\(studentCode)")!
        
        //Task for obtaining the json payload and reading the attendance data if received
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            let jsonData = try? JSONSerialization.jsonObject(with: data, options: [])
            
            if let dict = jsonData as? [String: Any] {
                if let errorDict = dict["error"] as? [String: Any] {
                    isAttending = errorDict["status"] as! String != self.NOT_FOUND
                } else {
                    isAttending = true
                }
            } else {
               isAttending = true
            }
            
            if(isAttending) {
                callback()
            }
        }
        
        task.resume()
    }
    
    //Uses callback to handle the student list result
    func getStudentList(_ cb: @escaping (_ list: [String: String]) -> Void) {
        let url = URL(string: self.API_ROUTE + "studentsList")!
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            let jsonData = try? JSONSerialization.jsonObject(with: data, options: [])
            var studentDict = [String: String]()
            
            if let dict = jsonData as? [String: Any] {
                if let studentList = dict["documents"] as? [[String: Any]] {
                    studentList.forEach({ (student) in
                        let res = self.getStudentCodeAndName(student)
                        studentDict[res.name] = res.code
                    })
                }
            }
            
            print(studentDict)
            cb(studentDict)
        }
        
        task.resume()
    }
    
    func getStudentCodeAndName(_ studentData: [String: Any]) -> (name: String, code: String) {
        if let fields = studentData["fields"] as? [String: Any] {
            guard let nameContainer = fields["name"] as? [String: String] else { return ("","") }
            guard let codeContainer = fields["code"] as? [String: String] else { return ("","") }
            return (nameContainer["stringValue"]!,codeContainer["stringValue"]!)
        }
        return ("","")
    }
}
