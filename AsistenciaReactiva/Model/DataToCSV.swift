//
//  DictionaryToCSV.swift
//  AsistenciaReactiva
//
//  Created by Jorge Andres Gomez Villamizar on 8/23/19.
//  Copyright © 2019 Jorge Andres Gomez Villamizar. All rights reserved.
//

import Foundation

class DataToCSV {
    func exportArrayToCSV(_ tableHeaders: String, _ rowsData: [(String, String, Bool)]) -> (URL, Bool) {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-M-yyyy"
        let dateString = formatter.string(from: date)
        
        let filename = "logForDate_\(dateString).csv"
        let path = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(filename)
        
        var csv = ""
        csv.append(tableHeaders)
        rowsData.forEach({row in
            csv.append("\n\(row.0),\(row.1),\(row.2 ? "IN CLASS" : "ABSENT")")
        })
        
        do {
            try csv.write(to: path, atomically: true, encoding: String.Encoding.utf8)
            return (path, true)
        } catch {
            print("CSV export failed")
        }
        
        return (path, false)
    }
}
