//
//  StudentListController.swift
//  AsistenciaReactiva
//
//  Created by Jorge Andres Gomez Villamizar on 8/22/19.
//  Copyright © 2019 Jorge Andres Gomez Villamizar. All rights reserved.
//

import UIKit

class StudentTableViewController: UITableViewController {
    let ABSENT = "ABSENT"
    let IN_CLASS = "IN CLASS"
    var fireBaseManager: FireBaseManager? = nil
    var dataToCSV: DataToCSV? = nil
    var studentsList: [(String,String,Bool)]? = nil
    @IBOutlet var studentTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        fireBaseManager = FireBaseManager()
        dataToCSV = DataToCSV()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.studentsList != nil) {
            return self.studentsList!.count + 3
        } else {
            return 3
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let lastIndex = tableView.numberOfRows(inSection: indexPath.section)
        
        if(indexPath.row == (lastIndex - 1)) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "backCell", for: indexPath)
            return cell
        } else if(indexPath.row == (lastIndex - 2)) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "exportCell", for: indexPath)
            return cell
        } else if(indexPath.row == (lastIndex - 3)) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "reloadCell", for: indexPath)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "student", for: indexPath)
            let student = studentsList![indexPath.row]
            if(student.2) {
                cell.textLabel?.text = "NAME: \(student.0) -- CODE: \(student.1) -- STATUS: \(IN_CLASS)"
                cell.backgroundColor = #colorLiteral(red: 0.395638674, green: 0.7647058964, blue: 0.4034307789, alpha: 1)
            } else {
                cell.textLabel?.text = "NAME: \(student.0) -- CODE: \(student.1) -- STATUS: \(ABSENT)"
                cell.backgroundColor = #colorLiteral(red: 1, green: 0.4487130457, blue: 0.4150264507, alpha: 1)
            }
            return cell
        }
    }
    
    @IBAction func reloadAllStudentData(_ sender: Any) {
        self.studentsList = [(String, String, Bool)]()
        
        let transformStudents: (_ dict: [String: String]) -> Void = { list in
            for (name, code) in list {
                self.studentsList?.append((name, code, false))
            }
            
            print("STUDENT LIST", self.studentsList!)
            //Mock data because firebase was down at the time of the development and I needed to test
            self.studentsList?.append(("Student A","32141213",true))
            self.studentsList?.append(("Student B","3098714213",false))
            self.studentsList?.append(("Student C","0392841213",true))
            
            DispatchQueue.main.async {
                self.studentTableView.reloadData()
            }
        }
        
        fireBaseManager?.getStudentList(transformStudents)
    }
    
    @IBAction func exportData(_ sender: Any) {
        let csvData = dataToCSV?.exportArrayToCSV("name,student code,status", studentsList!)
        if(csvData!.1) {
            let exportViewController = UIActivityViewController(activityItems: [csvData!.0], applicationActivities: [])
            self.present(exportViewController, animated: true, completion: nil)
            if let popOver = exportViewController.popoverPresentationController {
                popOver.sourceView = self.view
            }
        }
    }
}

