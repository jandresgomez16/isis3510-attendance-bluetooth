//
//  ViewController.swift
//  AsistenciaReactiva
//
//  Created by Jorge Andres Gomez Villamizar on 8/13/19.
//  Copyright © 2019 Jorge Andres Gomez Villamizar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    //MARK: Properties
    @IBOutlet weak var attendanceStatus: UILabel!
    @IBOutlet weak var toggleSearchButton: UIButton!
    @IBOutlet weak var serviceNameTag: UITextField!
    let searchingText = "Stop boradcast"
    let idleText = "Start broadcast"
    var statusBT = false
    var handlerBT: BTManager? = nil
    var fireBaseManager: FireBaseManager? = nil
    let codigoEstudiante = "201618492"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        handlerBT = BTManager()
        fireBaseManager = FireBaseManager()
        
    }

    //MARK: Actions
    @IBAction func toggleAttendanceSearch(_ sender: UIButton) {
        if(!statusBT) {
            var serviceName = self.serviceNameTag.text!
            if(serviceName == "") {
                serviceName = "Default Service"
            }
            
            handlerBT?.startDiscover(serviceName)
            statusBT = !statusBT
            toggleSearchButton.setTitle(searchingText, for: .normal)
        } else {
            handlerBT?.endDiscover()
            toggleSearchButton.setTitle(idleText, for: .normal)
            statusBT = !statusBT
        }
    }
    
    
    @IBAction func verifyAttendance(_ sender: UIButton) {
        let changeAttendanceStatus: () -> Void = {
            self.attendanceStatus.text = "Status: OK"
            self.attendanceStatus.textColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        }
        
        print("Is advertising? \(handlerBT!.isAdvertising())")
        fireBaseManager!.checkForValue(codigoEstudiante, changeAttendanceStatus)
        
        let sampleCallback: (_ list: [String: String]) -> Void = { list in 
            print("sample")
        }
        fireBaseManager!.getStudentList(sampleCallback)
    }
}

