Attendance App for Course ISIS 3510
----------------------------------------------
This is a very simple app thus far, it holds two main functionalities which are the following:
* **BLE Peripheral broadcasting:** the class BTManager at /Model/BTManager is in charge of broadcasting a BLE service with the name thats chosen inside the UI on the "Your service name here" label.
* **Attendance check:** with the button verify attendance, an HTTP request is sent from the class at /Model/FireBaseManager which verifies wether or not the request returned a 404 not found or else. This changed the status to green